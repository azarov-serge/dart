```dart
class Firestore {
  Map<String, dynamic> _database = {};
  Map<String, dynamic> _whereData = {};
  String _collectionName = '';

  Firestore(Map<String, dynamic> database) {
    this._database = database;
  }

  where(String field, String sign, dynamic value) {
    this._whereData.addAll({'field': field, 'sign': sign, 'value': value});
  }

  collection(String collectionName) {
    this._collectionName = collectionName;
  }

  get() {
    var field = this._whereData['field'];
    var sign = this._whereData['sign'] != null ? this._whereData['sign'] : '';
    var value = this._whereData['value'];
    var collection = this._database[this._collectionName];
    var snapshot = {};
    snapshot['docs'] = [];

    for(var i = 0; i < collection.length; i ++) {
      var valid = false;
      var doc = {};

      switch(sign) {
        case '==': {
					if (collection[i][field] == value) {
					  valid = true;
					}
				}
     		break;

        case '!=': {
					if (collection[i][field] != value) {
					  valid = true;
					}
				}
				break;

        case '<=': {
					if (collection[i][field] <= value) {
					  valid = true;
					}
				}
				break;

				case '>=': {
					if (collection[i][field] >= value) {
					  valid = true;
					}
				}
				break;

        case '<': {
					if (collection[i][field] < value) {
					  valid = true;
					}
				}
				break;

				case '>': {
					if (collection[i][field] > value) {
					  valid = true;
					}
				}
				break;

				default: { valid = true; }
        break;
      }

      if (valid) {
		doc['id'] = collection[i]['id'];
        Map<String, dynamic> collectionData = { ...collection[i] };
        collectionData.remove('id');
        doc['data'] = () => collectionData;
        snapshot['docs'].add(doc);
      }
    }

		this._whereData = {};
		this._collectionName = '';

    return snapshot;
	}
}


void main() {
  var test = [
    { 'id': '1', 'data': 'data1', 'value': 1 },
    { 'id': '2', 'data': 'data2', 'value': 2 },
    { 'id': '3', 'data': 'data3', 'value': 3 },
    { 'id': '4', 'data': 'data4', 'value': 4 },
    { 'id': '5', 'data': 'data5', 'value': 5 },
  ];
  var db = { 'test': test };
  var firesore = Firestore(db);

  firesore
    ..collection('test')
    ..where('value', '>', 2);

  var result1 = firesore.get();

  var result2 = firesore
    ..collection('test')
    ..where('value', '!==', 3)
    ..get();

  print('''
    result1 = $result1

    result2 = $result2
  ''');
}

```